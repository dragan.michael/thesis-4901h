"""
"""
## Imports
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import toolkit as thesis
import numpy as np
from sklearn import cluster

##
def main():
    dataset = thesis.get_data(describe=True)
    dataset = thesis.preprocess(dataset, describe=True)
    fig, ax = thesis.explore_kmeans(dataset)


main()

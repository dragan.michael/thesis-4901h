# COIS-4901H: Undergraduate Thesis

## Description
This is an advanced reading course. Students will, under faculty supervision, propose, develop and evaluate independent research in the area of data science.

## About
The breadth of research involving data analytics and astronomy is broad. This research project aims to facilitate the discovery of exoplanetary systems using intelligent scalable models.

## Setup
``` bash
pip install -r requirements.txt
```
## Data Sources
**Sloan Digital Sky Survey**
* [marvelStar](https://skyserver.sdss.org/dr12/en/help/browser/browser.aspx?cmd=description+marvelsStar+U#&&history=description+marvelsStar+U)
* [marvelsVelocityCurveUF1D](https://skyserver.sdss.org/dr12/en/help/browser/browser.aspx?cmd=description+marvelsVelocityCurveUF1D+U#&&history=description+marvelsVelocityCurveUF1D+U) 

**EU**
* [exoplanet.eu](http://exoplanet.eu/) 

**NASA**
* [Exoplanet Archive](https://exoplanetarchive.ipac.caltech.edu/)
* [openNASA](https://open.nasa.gov/open-data/)

## Example K-Means Clustering of Exoplanetary Systems
![K-Means Exoplanetary Systems](./img/kmeans_clusters_3.png)

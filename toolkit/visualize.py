import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

##
def explore_kmeans(df: pd.DataFrame) -> plt.Figure:
    """Exploring the raw exoplanet eu dataset.
    Args:
        df : pd.DataFrame # Dataset containing values to graph.
    Returns
        figure : plt.Figure object

    """

    # Instantiate Figures and Gridspecs
    figure = plt.figure(figsize=(11, 9))
    gs = figure.add_gridspec(6, 6)
    # Axes to plot on
    ax: plt.Axes = figure.add_subplot(gs[:4, :])
    # Scatterplot of the exoplanets' mass against orbital period
    sns.scatterplot(
        data=df,
        x="planet_mass_j_nasa_log",
        y="orbital_period_p_nasa_log",
        hue="K-Means Cluster",
        ax=ax,
    )

    # Setting Titles
    figure.suptitle(
        "Exoplanets: Classifying Exoplanets using K-Means Clustering", fontweight="bold"
    )
    ax.set_title("K-Means : Clusters = 3")
    ax.set_xlabel("Planetary Mass (Mjup) Natural Log Transformed")
    ax.set_ylabel("Orbital Period (in days) Natural Log Transformed")

    # Final aethetic changes
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    # Finishing Touches
    figure.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.savefig("./img/kmeans_clusters_3.png")
    return figure, ax


def story_board(df: pd.DataFrame) -> plt.Figure:
    """Create a storyboard.

    Args:
        df : DataFrame
        **kwargs:{  #TODO   }

    Returns:
        None

    """

    # Instantiate Figures and Grids
    figure: plt.Figure = plt.figure(figsize=(13, 9))
    gs = figure.add_gridspec(5, 4)

    figure.tight_layout(rect=[0, 0.03, 1, 0.95])

    return figure


def infograph(df: pd.DataFrame) -> plt.Figure:
    """Create a infograph.

    Args:
        df : DataFrame
        **kwargs:{  #TODO   }

    Returns:
        None

    """

    # Instantiate Figures and Grids
    figure: plt.Figure = plt.figure(figsize=(13, 9))
    gs = figure.add_gridspec(5, 4)

    figure.tight_layout(rect=[0, 0.03, 1, 0.95])

    return figure

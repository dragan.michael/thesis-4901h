import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
import scipy as sp
import sklearn

from sklearn import cluster


def get_data(describe=False) -> pd.DataFrame:
    """Retrieve raw data

    Data sources come from sloan, exoplanet.eu, and NASA.

    Args:

    Returns
        raw_data pd.DataFrame
    """
    # Getting the data from our three primary resources [nasa, exo_eu, sdss]
    exoplanet_eu = pd.read_csv("./cache/exoplanet_eu.csv", index_col="planet_name")
    exoplanet_eu.index = exoplanet_eu.index.map(lambda x: str(x).lower())
    nasa_composite = pd.read_csv(
        "./cache/nasa_composite.csv", index_col="planet_name"
    ).add_suffix("_nasa")
    nasa_composite.index = nasa_composite.index.map(lambda x: str(x).lower())
    # TODO marvels sdss

    df = pd.concat([exoplanet_eu, nasa_composite], axis=1).sort_index()
    # Log to console dataset information
    if describe == True:
        # Missing Values Information
        exoeu_na = exoplanet_eu.isna().sum().sort_values().rename("Exoplanet EU")
        nasa_comp_na = (
            nasa_composite.isna().sum().sort_values().rename("Nasa Composite")
        )
        missing_data_counts = pd.concat([exoeu_na, nasa_comp_na], axis=1)
        print(
            "----- Exoplanet Eu Catalogue Dataset -----",
            f"\n# of records: {exoplanet_eu.index.size}",
            f"\n# of columns: {exoplanet_eu.columns.size}",
            f"\n# of missing values: {exoeu_na.sum()}",
            "\n----- Nasa Composite Dataset -----",
            f"\n# of records: {nasa_composite.index.size}",
            f"\n# of columns: {nasa_composite.columns.size}",
            f"\n# of missing values: {nasa_comp_na.sum()}",
            f"\nNumber of Missing Values Per Column: \n{missing_data_counts}",
            "\n\n------ { nasa_composite + exoplanet_eu } ------",
            f"\n{df.head()}",
            f"\n# of records {df.index.size}",
            f"\n# of columns {df.columns.size}",
        )

    return df


def preprocess(raw_data: pd.DataFrame, describe=False) -> pd.DataFrame:
    df = raw_data.copy()
    # Columns we're going to scale

    columns = ["orbital_period_p_nasa", "planet_mass_j_nasa"]
    columns_log = list(map(lambda x: x + "_log", columns))

    df1 = df[columns]  # .apply(np.log)
    # Remove outliers
    df1 = df1[df1 <= df1.quantile(0.99)]
    df1: pd.DataFrame = df1[df1 >= df1.quantile(0.01)]
    # Columns to log Transform
    df1 = pd.concat([df1, df1.apply(np.log)], axis=1)
    df1.columns = columns + columns_log
    df1["source"] = "NASA Composite"  # Temporarily in here
    df1.dropna(inplace=True)
    # KMeans
    kmeans = cluster.KMeans(n_clusters=3).fit(df1[columns_log])
    df1["K-Means Cluster"] = kmeans.labels_

    if describe == True:
        print("\n--- Preprocessing Steps ---")
        print("--- Transformed Columns --- ", f"Natural Log Transform {columns_log}")
        print("Transformed Dataframe: First five rows")
        print(df1.head())
        print(f"Number of Records after clustering {df1.index.size}")

    return df1

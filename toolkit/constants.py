NASA_COMP_COLUMNS = [
    "planet_name",  # str
    "host_name",  # str
    "number_stars",  # int
    "number_planets",  # int
    "discovery_method",  # str
    "year_discovered",  # int
    "discovery_facility",  # str
    "controversial_confirmation",  # int { 0, 1 }
    "orbital_period_p",  # int ;  period in days
    "planet_radius_e",  # float ; unit of measurement: 1 earth radius
    "planet_radius_j",  # float ; unit of measurement: 1 jupiter radius
    "planet_mass_e",  # float ; unit of measurement: earths
    "planet_mass_j",  # float ; unit of measurement: jupiters
    "planet_eccentricity",  # float
    "equilibrium_temp_k",  # float ; unit of measurement: kelvins
    "distance_to_system",  # float ; 1 AU
    "source",  # NASA_COMP || NASA_EXO || EXOPLANET_EU || MARVELS_STAR
]
EU_COLUMNS = [
    "planet_name",  # str
    "planet_status",
    "planet_mass_j",
    "planet_radius_j",
    "orbital_period",
    "planet_eccentricity",
    "planet_inclination",
    "angular_distance",
    "calculated_temperature",
    "measured_temperature",
    "discovery_method",
    "mass_detection_type",
    "radius_detection_type",
    "host_name",
]
